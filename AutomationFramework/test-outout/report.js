$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("file:/C:/Framework/AutomationFramework/src/test/resources/Features/Bet.feature");
formatter.feature({
  "name": "validate odds and returns offered",
  "description": "",
  "keyword": "Feature"
});
formatter.scenarioOutline({
  "name": "validate odds and returns offered for a football match",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@hook"
    }
  ]
});
formatter.step({
  "name": "I am WH customer on William Hill website",
  "keyword": "Given "
});
formatter.step({
  "name": "I choose Football event",
  "keyword": "And "
});
formatter.step({
  "name": "I enter \"\u003cbetAmount\u003e\" bet amount on home team to win a match",
  "keyword": "When "
});
formatter.step({
  "name": "I am displayed with correct odds and returns offered",
  "keyword": "Then "
});
formatter.examples({
  "name": "",
  "description": "",
  "keyword": "Examples",
  "rows": [
    {
      "cells": [
        "betAmount"
      ]
    },
    {
      "cells": [
        "0.05"
      ]
    }
  ]
});
formatter.scenario({
  "name": "validate odds and returns offered for a football match",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@hook"
    }
  ]
});
formatter.step({
  "name": "I am WH customer on William Hill website",
  "keyword": "Given "
});
formatter.match({
  "location": "WilliamHillHomePage.i_am_WH_customer_on_William_Hill_website()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "I choose Football event",
  "keyword": "And "
});
formatter.match({
  "location": "WilliamHillHomePage.i_choose_Football_event()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "I enter \"0.05\" bet amount on home team to win a match",
  "keyword": "When "
});
formatter.match({
  "location": "WilliamHillHomePage.i_enter_bet_amount_on_home_team_to_win_a_match(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "I am displayed with correct odds and returns offered",
  "keyword": "Then "
});
formatter.match({
  "location": "WilliamHillHomePage.i_am_displayed_with_correct_odds_and_returns_offered()"
});
formatter.result({
  "status": "skipped"
});
});