package com.williamHill.automation.webDriver;

import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.williamHill.automation.utils.PropertiesFile;

public class TestBase {

	PropertiesFile propertiesFile = new PropertiesFile();

	public static WebDriver driver;
	public static FluentWait<WebDriver> wait;

	public void initialization() {
		String browserName = propertiesFile.readValue("browser");
		if (browserName.equalsIgnoreCase("chrome")) {
			System.setProperty("webdriver.chrome.driver", "src/test/resources/Driver/chromedriver.exe");
			if (driver == null) {
				driver = new ChromeDriver();
			}
		} else if (browserName.equalsIgnoreCase("firefox")) {
			System.setProperty("webdriver.firefox.driver", "src/test/resources/Driver/chromedriver.exe");
			driver = new FirefoxDriver();
		}
		driver.manage().window().maximize();
		wait = new WebDriverWait(driver, 20).ignoring(StaleElementReferenceException.class);
	}

}
