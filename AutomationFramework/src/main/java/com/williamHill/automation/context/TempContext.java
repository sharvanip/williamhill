package com.williamHill.automation.context;

import java.util.HashMap;
import java.util.Map;

public class TempContext {
	
	public static Map<String, String> tempValues = new HashMap<String, String>();

	public String getTempValues(String key) {
		return tempValues.get(key).toString();
	}

	public void setTempValues(String key, String value) {
		tempValues.put(key, value);
	}

}
