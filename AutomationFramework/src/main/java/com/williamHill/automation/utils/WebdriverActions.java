package com.williamHill.automation.utils;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;

import com.williamHill.automation.webDriver.TestBase;

public class WebdriverActions extends TestBase {

	// Grouping actions performed on Webelemts
	// javascript select for reuseablity
	public void javaScriptClick(WebElement element) {
		try {
			JavascriptExecutor js = (JavascriptExecutor) driver;
			js.executeScript("arguments[0].click();", element);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
