package com.williamHill.automation.utils;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.chrome.ChromeDriver;

public class PropertiesFile {
	public static Properties prop;


	public PropertiesFile() {
		//reading the the config file to fetch properties
		try {
			prop = new Properties();
			FileInputStream fis = new FileInputStream(
					"src/test/resources/config.properties");
			prop.load(fis);
		} catch (IOException e) {
			e.getMessage();
		}
	}
	//initialization the browser and getting URL to launch
	public String readValue(String key) {
		String Value = prop.getProperty(key);
		return Value;
	}

}
