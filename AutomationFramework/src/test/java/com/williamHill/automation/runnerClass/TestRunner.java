package com.williamHill.automation.runnerClass;

import org.junit.runner.RunWith;
import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;

@RunWith(Cucumber.class)
@CucumberOptions( 
		features = "src/test/resources/features", //the path of the feature files
		glue={"com/williamHill/automation/stepDef"},//the path of the step definition files
		tags = {"@systemTest"},
		plugin= {"pretty","html:target/test-outout", "json:target/json_output/cucumber.json", "junit:target/junit_xml/cucumber.xml"}, //to generate different types of reporting
		monochrome = true, //display the console output in a proper readable format
		strict = true, //it will check if any step is not defined in step definition file
		dryRun = true //to check the mapping is proper between feature file and step def file

		)
public class TestRunner {
}


