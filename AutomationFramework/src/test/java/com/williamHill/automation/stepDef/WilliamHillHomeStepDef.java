package com.williamHill.automation.stepDef;

import org.junit.Assert;

import com.williamHill.automation.context.TempContext;
import com.williamHill.automation.missions.BettingMission;
import com.williamHill.automation.pages.BettingPage;
import com.williamHill.automation.utils.PropertiesFile;
import com.williamHill.automation.webDriver.TestBase;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class WilliamHillHomeStepDef extends TestBase {

	TempContext context = new TempContext();
	BettingMission bettingMission = new BettingMission();
	PropertiesFile propertiesFile = new PropertiesFile();
	BettingPage home = new BettingPage();

	// Launching the sports betting site
	@Given("I am WH customer on William Hill website")
	public void i_am_WH_customer_on_William_Hill_website() {
		System.out.println("I am on the William Hill website");
	}

	// Choosing the football game
	@Given("I choose Football event")
	public void i_choose_Football_event() {
		home.selectGame();

	}

	// selecting the first event available in "English Premier League” and odds of
	// the home team
	@When("I enter {string} on home team to win a match")
	public void i_enter_on_home_team_to_win_a_match(String betAmount) {
		bettingMission.enterBetOnFirstAvailableTeam(betAmount);
	}

	// Verification of correct odds and returns on the betslip
	@Then("I am displayed with correct odds and returns offered")
	public void i_am_displayed_with_correct_odds_and_returns_offered() {
		Assert.assertEquals("InCorrect odds are returned from bestslip", context.getTempValues("odd"),
				home.oddsFeatched.getText());
		bettingMission.calcualtedReturn();
		Assert.assertEquals("InCorrect returns are returned from bestslip", context.getTempValues("slipReturn").trim(),
				context.getTempValues("calcualtedReturn"));
	}

}
