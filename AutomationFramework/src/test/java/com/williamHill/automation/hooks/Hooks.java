package com.williamHill.automation.hooks;

import java.util.concurrent.TimeUnit;

import com.williamHill.automation.utils.PropertiesFile;
import com.williamHill.automation.utils.TestUtil;
import com.williamHill.automation.webDriver.TestBase;

import cucumber.api.java.After;
import cucumber.api.java.Before;

public class Hooks {

	PropertiesFile propertiesFile = new PropertiesFile();
	TestBase browser = new TestBase();


@Before("@hook-test")
public void openWebSite() throws Throwable {
	browser.initialization();
	browser.driver.get(propertiesFile.readValue("url"));
	browser.driver.manage().timeouts().implicitlyWait(TestUtil.IMPLICIT_WAIT, TimeUnit.SECONDS);
}

@After("@hook-test")
public void clearCookies() throws Exception {
	System.out.println("enter after hooks");
	browser.driver.manage().deleteAllCookies();
	browser.driver.close();
	browser.driver.quit();
}

}
