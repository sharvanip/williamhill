package com.williamHill.automation.pages;

import java.util.List;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import com.williamHill.automation.context.TempContext;
import com.williamHill.automation.utils.WebdriverActions;
import com.williamHill.automation.webDriver.TestBase;

public class BettingPage extends TestBase {

	TempContext context = new TempContext();
	WebdriverActions Actions = new WebdriverActions();

	public static String odd = null;
	public static String betValue = null;
	public static String returns = null;
	public static String Odds_On_slip = null;

	// Initializing the page Objects
	public BettingPage() {
		PageFactory.initElements(driver, this);
	}

	// Page Factory :
	@FindBy(xpath = "//a[@title='Football']")
	private WebElement footballGame;

	@FindBy(xpath = "//ul[@class='btmarket__content-margins']//li//a[contains(@href,'/betting/en-gb/football')]")
	private List<WebElement> ListEvent;

	@FindBy(xpath = "//div[@class='btmarket__selection']//button")
	private List<WebElement> ListOdd; // betbutton__odds

	@FindBy(xpath = "//div[@class='betslip-selection__stake-container betslip-selection__stake-container--single']"
			+ "//span//input[contains(@id,'stake-input')]")
	private WebElement bettingValue;

	@FindBy(xpath = "//header[@class='betslip-selection__header betslip-selection__header--single']//span//selection-price")
	public WebElement oddsFeatched;

	@FindBy(xpath = "//div[@class='betslip-selection__data betslip-selection__data--single']"
			+ "//div//span[@class='betslip-selection__estimated-returns-amount betslip-label--placeholder']")
	private WebElement returnsFeatched;

	// Actions
	// Selecting the football event
	public void selectGame() {
		wait.until(ExpectedConditions.elementToBeClickable(footballGame)).click();
	}

	// Selecting first event in "English Premier League”
	public void selectFirstEvent() {
		try {
			Thread.sleep(3000); // StaleElementReferenceException should be handled for uninterrupted flow
								// instead using threadsleep
			Actions.javaScriptClick(ListEvent.get(0));
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	// Selecting home odd by picking up the first odd available
	public void selectHomeodds() {
		try {
			Thread.sleep(8000);
			context.setTempValues("odd", ListOdd.get(0).getText());
			Actions.javaScriptClick(ListOdd.get(0));
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	// Entering the bet value
	public void betValue(String value) {
		context.setTempValues("betValue", String.valueOf(value));
		bettingValue.sendKeys(value);
		context.setTempValues("slipReturn", returnsFeatched.getText());
		context.setTempValues("Odds_On_slip", oddsFeatched.getText());
	}
}
