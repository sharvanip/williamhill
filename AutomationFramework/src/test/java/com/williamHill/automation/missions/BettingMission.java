package com.williamHill.automation.missions;

import java.math.BigDecimal;
import java.math.RoundingMode;
import com.williamHill.automation.context.TempContext;
import com.williamHill.automation.pages.BettingPage;

public class BettingMission {

	BettingPage bettingPage = new BettingPage();
	TempContext context = new TempContext();

	public void enterBetOnFirstAvailableTeam(String value) {
		System.out.println("select first event");
		bettingPage.selectFirstEvent();
		bettingPage.selectHomeodds();
		bettingPage.betValue(value);

	}

	// calculation of return value on betslip
	public void calcualtedReturn() {
		String[] oddsnum = context.getTempValues("odd").split("/");
		BigDecimal BigDecimalnum = new BigDecimal(oddsnum[0]);
		BigDecimal BigDecimaldenom = new BigDecimal(oddsnum[1]);
		BigDecimal wholenum = BigDecimalnum.divide(BigDecimaldenom, 4, RoundingMode.CEILING);
		BigDecimal Value = new BigDecimal(context.getTempValues("betValue"));
		BigDecimal calculated_return = ((wholenum.multiply(Value)).add(Value));
		BigDecimal Round_calculated_return = calculated_return.setScale(2, BigDecimal.ROUND_FLOOR);
		context.setTempValues("calcualtedReturn", Round_calculated_return.toString());
	}
}
