@hook-test
Feature: validate odds and returns offered

@systemTest
#with Examples Keyword
Scenario Outline: validate odds and returns offered for a football match

Given I am WH customer on William Hill website
And I choose Football event
When I enter "<betAmount>" on home team to win a match
Then I am displayed with correct odds and returns offered

Examples:
|betAmount|
|0.05 |
